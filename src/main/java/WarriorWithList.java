import java.util.List;
import java.util.Random;

public class WarriorWithList {

    private int level = 0;
    private Random random;
    private List damageHistory;

    public WarriorWithList(){
    }

    public void levelUp() {
        level++;
    }

    public double calculateDamage() {
        double damage = random.nextDouble() * level;
        this.damageHistory.add(damage);
        return damage;
    }
}
