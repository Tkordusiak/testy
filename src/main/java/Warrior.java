import java.util.Random;

public class Warrior {

    private int level = 0;
    private Random random;

    public Warrior(Random random){
        this.random = random;
    }

    public void levelUp() {
        level++;
    }

    public double calculateDamage() {
        return random.nextDouble() * level;
    }
}
