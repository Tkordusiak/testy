import java.util.stream.Stream;

public class Greeter {

    public static String greet() {
        return "Hello my friend";
    }

    public static String greet(String... names) {
        if (names.length == 0) {
            return "Hello my firiend";
        }
        if (names.length == 1) {
            String name = names[0];
            if (names.equals(name.toUpperCase())) {
                return String.format("HELLO %s!", names);
            }
            return String.format("Hello %s.", names);
        }
        if (names.length == 2) {
            return String.format("Hello %s and %s.", names[0], names[1]);
        }
        return "";
    }

}
