import java.nio.file.attribute.UserPrincipalNotFoundException;

public class Account {

    private String name;
    private String surname;
    private double accountBalance;
    private String accountNumber;

    public Account(String name, String surname, double accountBalance, String accountNumber){
        this.name = name;
        this.surname = surname;
        this.accountBalance = accountBalance;
        this.accountNumber = accountNumber;
    }

    public String getOwner(){
        return name + " " + surname;
    }

    public double getBalance(){
        return this.accountBalance;
    }

    public String getAccountNumber(){
        return accountNumber;
    }

    public void transfer(double transferAmount){
        this.accountBalance += transferAmount;
    }

    public boolean sendMoney(Account account, double amount) throws UserIsRetardedException {
        if (this == account){
            throw new UserIsRetardedException("Are you a nut?!");
        }
        if (amount >= 0 && amount <= accountBalance){
            this.transfer(-amount);
            account.transfer(amount);
            return true;
        }
        return false;
    }
}
