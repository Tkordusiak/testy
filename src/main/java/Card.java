public class Card {

    private Account account;
    private String pin;
    private int counter = 0;

    public Card(Account account, String fourDigitPinNumber) throws Exception {
        if (!validatePinNumber(fourDigitPinNumber)) {
            throw new InvalidPinException("pin not valid, cannot create card!");
        }
        this.account = account;
        this.pin = fourDigitPinNumber;
    }

    private boolean validatePinNumber(String pin) {
        return (pin.matches("[0-9]+") && pin.length() == 4);
    }

    public Account getAccount() {
        return this.account;
    }

    public String getOwner() {
        return this.account.getOwner();
    }

    public boolean checkPin(String givenPin) throws CardLockExeption {

        if (givenPin.equals(this.pin)) {
            counter = 0;
            return true;
        }

        if (counter >= 3){
            throw new CardLockExeption("message");
        }

        if (!givenPin.equals(this.pin)) {
            counter ++;
            if (counter == 3){
                throw new CardLockExeption("message");
            }
            return false;
        }

        return this.pin.equals(givenPin);
    }

    public boolean withdrawFromAtm(String pin, double amount) throws CardLockExeption {
        if (checkPin(pin) && amount > 0 && account.getBalance() >= amount) {
            account.transfer(-amount);
            return true;
        }
        return false;
    }

}
