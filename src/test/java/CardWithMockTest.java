import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

import java.security.PublicKey;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


public class CardWithMockTest {

    private Account account;
    private Card card;

    @BeforeEach
    public void setUp() throws Exception {
        account = Mockito.mock(Account.class);
        card = new Card(account, "1234");
    }

    @Test
    public void test1() throws Exception {

        Assertions.assertTrue(card.checkPin("1234"));
        assertThat(card.checkPin("1234")).isTrue();
    }


    @Test
    public void test2() throws Exception {

        Assertions.assertFalse(card.checkPin("0000"));
        assertThat(card.checkPin("0000")).isFalse();
    }

    @Test
    public void testWithdrawMoney() throws CardLockExeption {
        Mockito.when(account.getBalance()).thenReturn(10000000.00);

        assertThat(card.withdrawFromAtm("1234", 1000)).isTrue();

    }

    @Test
    public void testWithdrawMoney2() throws CardLockExeption {
        Mockito.when(account.getBalance()).thenReturn(10000000.00);

        assertThat(card.withdrawFromAtm("0000", 1000)).isFalse();

    }

    @Test
    public void testGetAccount() {
        assertThat(card.getAccount()).isSameAs(account);
    }

    @Test
    public void testGetOwner() {
        Mockito.when(card.getOwner()).thenReturn("Jan Kowalski");
        assertThat(card.getOwner()).isEqualTo("Jan Kowalski");
    }

    @Test
    public void testShouldThrowExeption() {
        Assertions.assertThrows(InvalidPinException.class, () -> new Card(account, "messagew"));

    }

    // TESTY SPARAMETRYZOWANE
    public static Stream<Arguments> dataFeeder() {
        return Stream.of(
                Arguments.of("1234", true),
                Arguments.of("0000", false)
        );
    }

    @ParameterizedTest
    @MethodSource("dataFeeder")
    public void paramTestCheckPin(String pin, boolean result) throws CardLockExeption {
        assertThat(card.checkPin(pin)).isEqualTo(result);

    }

    @ParameterizedTest
    @ValueSource(strings = {"0000", "1111", "2222", "4444"})
    public void paramsTestFalseCheckPin(String pin) throws CardLockExeption {
        assertThat(card.checkPin(pin)).isFalse();
    }

    // Jako bankomat chcę zablokować kartę w przypadku 3 prób niepoprawnie wpisanego pinu.
    // blokada przy 3 próbie złego pin
    // blokada tylko gdy 3 próby pod rząd
    // modyfikaja checkPin
    // rzucenie błędu

    @Test
    public void shouldLockCardWhenInvalidAttempts() throws CardLockExeption {
        card.checkPin("0000");
        card.checkPin("0000");

        Assertions.assertThrows(CardLockExeption.class, () -> card.checkPin("0000"));
    }

    @Test
    public void shouldNotLockWhenAttemptsInRow() throws CardLockExeption {
        card.checkPin("0000");
        card.checkPin("0000");
        card.checkPin("1234");
        assertThat(card.checkPin("0000")).isFalse();
    }
}
