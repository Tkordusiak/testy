import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class Warrior2Test {

    @InjectMocks
    Warrior warrior;

    @Mock
    Random mockedRandom;

    @Test
    public void testNewWay(){
        Mockito.when(mockedRandom.nextDouble()).thenReturn(0.5);

        warrior.levelUp();

        assertThat(warrior.calculateDamage()).isEqualTo(0.5);
        assertThat(warrior.calculateDamage()).isEqualTo(0.5);
        assertThat(warrior.calculateDamage()).isEqualTo(0.5);

        Mockito.verify(mockedRandom,
                Mockito.times(3)).nextDouble();
    }
}
