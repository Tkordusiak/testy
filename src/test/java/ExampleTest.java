import org.junit.jupiter.api.*;

public class ExampleTest {

    private Account account;

    @BeforeAll
    public static void classSetup(){
        System.out.println("Class setup");
    }

    @AfterAll
    public static void classTeardown(){
        System.out.println("Class teardown");
    }

    @BeforeEach
    public void setUp(){
        account = new Account("John", "Doe", 15000.00, "12345");
        System.out.println("setup");
    }

    @AfterEach
    public void tearDown(){
        System.out.println("tearDown");
    }

    @Test
    public void test1(){
        account.getBalance();
        System.out.println("running test1");
    }

    @Test
    public void test2(){
        account.getOwner();
        Assertions.fail("because thats why");
        System.out.println("running test2");
    }
}
