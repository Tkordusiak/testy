import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

public class CardTest {

    private Account account;
    private Card card;

    @BeforeEach
    public void setup() throws Exception {
        account = new Account("John", "Doe", 15000, "12345");
        card = new Card(account, "1234");
    }

    @Test
    public void test1() {
        Assertions.assertEquals("John Doe", card.getOwner(), "Owner of the card does not match");
    }

    @Test
    public void test2() throws CardLockExeption {
        Assertions.assertTrue(card.checkPin("1234"));
        assertThat(card.checkPin("1234")).isTrue();
    }

    @Test
    public void test3() throws CardLockExeption {
        Assertions.assertFalse(card.checkPin("0000"));
        assertThat(card.checkPin("0000")).isFalse();
    }

    @Test
    public void test4() {
        Assertions.assertSame(account, card.getAccount(), "Account should be the same");
        assertThat(card.getAccount()).as("Account should be the same").isSameAs(account);
    }

    @Test
    public void test5() throws Exception {
        try {
            Card newCard = new Card(account, "asdb");
            fail("Exception should be thrown");
        } catch (InvalidPinException e) {
            Assertions.assertEquals("pin not valid, cannot create card!", e.getMessage());
        }
    }

    @Test
    public void test6() throws Exception {
        Exception e = Assertions.assertThrows(InvalidPinException.class, () -> new Card(account, "asdb"));
        Assertions.assertEquals("pin not valid, cannot create card!", e.getMessage());
    }

    //Jako posiadacz konta chcę wypłacić pieniądze z bankomatu.
    // Card.withdrawFromAtm(String pin, Double amount)
    // outcome:
    // saldo zmniejszone o kwote
    // true - udalo sie, false - nie udalo sie

    // test sprawdzenie salda konta
    // metoda nie powinna dac mozliwosci wplaty pieniedzy

    @Test
    public void shouldBeAbleToWithdrawMoney() throws CardLockExeption {
        boolean transactionStatus = card.withdrawFromAtm("1234", 15000);
        double newAccountBalance = card.getAccount().getBalance();

        assertThat(transactionStatus).as("Should withdraw money").isTrue();
        assertThat(newAccountBalance).isEqualTo(0);
    }

    @Test
    public void shouldNotWithdrawWhenPinInvalid() throws CardLockExeption {
        boolean transactionStatus = card.withdrawFromAtm("0000", 15000);
        double newAccountBalance = card.getAccount().getBalance();

        assertThat(transactionStatus).as("Should Not withdraw money").isFalse();
        assertThat(newAccountBalance).isEqualTo(15000);
    }

    @Test
    public void shouldNotWithdrawWhenInsufficientFunds() throws CardLockExeption {
        boolean transactionStatus = card.withdrawFromAtm("1234", 15001);
        double newAccountBalance = card.getAccount().getBalance();

        assertThat(transactionStatus).as("Should Not withdraw money").isFalse();
        assertThat(newAccountBalance).isEqualTo(15000);
    }

    @Test
    public void shouldNotAcceptNegativeAmount() throws CardLockExeption {
        boolean transactionStatus = card.withdrawFromAtm("1234", -1000);
        double newAccountBalance = card.getAccount().getBalance();

        assertThat(transactionStatus).as("Should Not withdraw money").isFalse();
        assertThat(newAccountBalance).isEqualTo(15000);
    }



}
