import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.attribute.UserPrincipalNotFoundException;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;


public class AccountTest {

    private Account account;

    @BeforeEach
    public void setup() {
        account = new Account("John", "Doe", 15000, "12345");
    }

    @Test
    public void testShouldReturnProperOwnerName() {
        assertEquals("John Doe", account.getOwner(), "Owner name should match");
        assertThat(account.getOwner()).as("Owner name should match").isEqualTo("John Doe");
    }

    @Test
    public void testShouldReturnProperBalance() {
        assertEquals(15000, account.getBalance(), "Balance should match");
    }

    @Test
    public void testShouldReturnProperAccNo() {
        assertEquals("12345", account.getAccountNumber(), "Account number should match");
    }

    @Test
    public void testShouldProperlyTransferMoney() {

        account.transfer(50);
        assertEquals(15050, account.getBalance(), "Balance should decrease");
    }

    //    Jako posiadacz konta chcę przelać pieniądze na swoje konto z innego konta.
    //    metoda: Account.sendMoney(Account receiver, double amount)
    //    1. uniemozliwic przesyl w druga strone, to nie socjalizm
    //    2. sprawdzic czy mamy pieniadze potrzebne na transfer
    //    3. jesli sie udalo, zwroc true, a jak nie to false
    //    4. oba salda powinny ulec zmianie, jesli transfer mozliwy

    @Test
    public void testShouldProperlySendMoneyToOtherAccount() throws UserIsRetardedException {
        Account account2 = new Account("Jane", "Doe", 0, "54321");

        boolean status = account.sendMoney(account2, 1000);
        double account1Balance = account.getBalance();
        double account2Balance = account2.getBalance();

        assertThat(status).isTrue();
        assertThat(account1Balance).isEqualTo(14000);
        assertThat(account2Balance).isEqualTo(1000);
    }

    @Test
    public void testShouldNotSendMoneyToOtherAccountWhenInsufficientFunds() throws UserIsRetardedException {
        Account account2 = new Account("Jane", "Doe", 0, "54321");

        boolean status = account.sendMoney(account2, 16000);
        double account1Balance = account.getBalance();
        double account2Balance = account2.getBalance();

        assertThat(status).isFalse();
        assertThat(account1Balance).isEqualTo(15000);
        assertThat(account2Balance).isEqualTo(0);
    }

    @Test
    public void testShouldNotSendMoneyToOtherAccountWhenAmountIsNegative() throws UserIsRetardedException {
        Account account2 = new Account("Jane", "Doe", 0, "54321");

        boolean status = account.sendMoney(account2, -1600);
        double account1Balance = account.getBalance();
        double account2Balance = account2.getBalance();

        assertThat(status).isFalse();
        assertThat(account1Balance).isEqualTo(15000);
        assertThat(account2Balance).isEqualTo(0);
    }

    @Test
    public void testShouldThrowExceptionWhenReceiverIsSameAsSender() {
        double account1Balance = account.getBalance();

        Exception e = Assertions.assertThrows(UserIsRetardedException.class,
                () -> account.sendMoney(account, 5000));
        assertThat(e.getMessage()).isEqualTo("Are you a nut?!");
        assertThat(account1Balance).isEqualTo(15000);

    }
}
