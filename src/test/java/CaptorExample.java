import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class CaptorExample {

    @InjectMocks
    WarriorWithList warrior;

    @Mock
    List damageHistory;

    @Mock
    Random random;

    @Captor
    ArgumentCaptor argCaptor;

    @Test
    public void whenUseCaptorAnnotation_thenTheSam() {
        Mockito.when(random.nextDouble()).thenReturn(1.00);

        warrior.levelUp();
        warrior.levelUp();
        warrior.levelUp();

        warrior.calculateDamage();
        Mockito.verify(damageHistory).add(argCaptor.capture());

        assertEquals(3.00, argCaptor.getValue());
    }
}
