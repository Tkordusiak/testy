import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

public class WarriorTest {

    @Test
    public void testOldFashionedMethod(){
        Random mockedRandom = Mockito.mock(Random.class);
        Mockito.when(mockedRandom.nextDouble()).thenReturn(0.5);

        Warrior warrior = new Warrior(mockedRandom);

        warrior.levelUp();

        assertThat(warrior.calculateDamage()).isEqualTo(0.5);
        assertThat(warrior.calculateDamage()).isEqualTo(0.5);
        assertThat(warrior.calculateDamage()).isEqualTo(0.5);

        Mockito.verify(mockedRandom, Mockito.times(3)).nextDouble();
    }
}
