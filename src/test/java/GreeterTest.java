import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class GreeterTest {

    @Test
    public void shouldGreetPerson(){
        assertThat(Greeter.greet("Janusz")).isEqualTo("Hello Janusz.");

    }

    @Test
    public void shouldWhenNoNameGiven(){
        assertThat(Greeter.greet()).isEqualTo("Hello my friend");

    }

    @Test
    public void shouldShout(){
        assertThat(Greeter.greet("GRAŻYNA")).isEqualTo("Hello GRAŻYNA.");
    }

    @Test
    public void shouldGreetMultiplePerson(){
        assertThat(Greeter.greet("Ania", "Artur")).isEqualTo("Hello Ania and Artur.");
    }
}